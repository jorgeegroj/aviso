class Patient < ApplicationRecord
  has_many :pros
  validates :doctor_id, presence: true
  has_one :doctor
  belongs_to :user
end
