json.extract! pro, :id, :patient_id, :q1, :q2, :integer, :q3, :q4, :q5, :created_at, :updated_at
json.url pro_url(pro, format: :json)
