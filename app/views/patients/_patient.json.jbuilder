json.extract! patient, :id, :doctor_id, :name, :address, :dob, :comment, :email, :phone, :tech_ability, :created_at, :updated_at
json.url patient_url(patient, format: :json)
