json.extract! doctor, :id, :name, :address, :phone, :title, :email, :experience, :created_at, :updated_at
json.url doctor_url(doctor, format: :json)
