class ProMailer < ApplicationMailer
  default from: 'notifications@aviso.com'

  def welcome_email
    @user = params[:user]
    @url  = params[:url]
    mail(to: @user.email, subject: 'Welcome to AVISO')
  end

  def alerta_pro
    @user = params[:user]
    @patient = params[:patient]
    @pro = params[:pro]
    @doctor = params[:doctor]
    mail(to: @user.email, subject: 'Alerta ROJA! Paciente')
  end

  def reminder_pro
    @user = params[:user]
    @patient = params[:patient]
    mail(to: @user.email, subject: 'Encuesta Semanal')
  end

  def info_pro
    @user = params[:user]
    @patient = params[:patient]
    @pro = params[:pro]
    mail(to: @user.email, subject: 'Recomendaciones basadas en sus respuestas')
  end
end
