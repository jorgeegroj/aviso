class CreatePatients < ActiveRecord::Migration[5.1]
  def change
    create_table :patients do |t|
      t.integer :doctor_id
      t.string :name
      t.text :address
      t.text :dob
      t.text :comment

      t.timestamps
    end
  end
end
