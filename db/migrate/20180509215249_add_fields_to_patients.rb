class AddFieldsToPatients < ActiveRecord::Migration[5.1]
  def change
    add_column :patients, :email, :string
    add_column :patients, :phone, :string
    add_column :patients, :tech_ability, :integer
  end
end
