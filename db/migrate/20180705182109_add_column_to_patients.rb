class AddColumnToPatients < ActiveRecord::Migration[5.1]
  def change
    add_column :patients, :diagnostic, :string
    add_column :patients, :estadio, :string
    add_column :patients, :treatment, :string
  end
end
