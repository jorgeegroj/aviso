class AddColumnToPro < ActiveRecord::Migration[5.1]
  def change
    add_column :pros, :current, :string
    add_column :pros, :others, :string
    add_column :pros, :severity, :integer
  end
end
