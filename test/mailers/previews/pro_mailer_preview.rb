# Preview all emails at http://localhost:3000/rails/mailers/pro_mailer
class ProMailerPreview < ActionMailer::Preview
  def welcome_email
    ProMailer.with(user: User.first).welcome_email
  end

  def alerta_pro
    ProMailer.with(user: User.first, patient: Patient.first, pro: Pro.first, doctor: Doctor.first).alerta_pro
  end
end
