# Preview all emails at http://localhost:3000/rails/mailers/mailer_mailer
class MailerMailerPreview < ActionMailer::Preview
  def welcome_email
    MailerMailer.with(user: User.first).welcome_email
  end
end
