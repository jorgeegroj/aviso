Rails.application.routes.draw do
  resources :doctors
  resources :patients
  resources :pros
  resources :users
  resources :user_sessions, only: [ :new, :create, :destroy ]

  get 'login'  => 'user_sessions#new'
  get 'logout' => 'user_sessions#destroy'

  get '/:page'=> 'static#show'


  root to: 'static#home'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
